include $(call first-makefiles-under,$(LOCAL_PATH))

PRODUCT_COPY_FILES += \
    vendor/extras/etc/permissions/org.codeaurora.snapcam.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/permissions/org.codeaurora.snapcam.xml \

PRODUCT_PACKAGES += \
    MotoWidget \
    MotoPhone \
    LMC
